import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { ActivatedRoute, Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AfflinkService {
  defaultPid = '137192';
  affid: any;
  constructor(
    private storage: Storage,
  ) {
    this.init();
  }

  async init() {
    const storage = await this.storage.create();
  }

  public async processAffidFromRoute(route: ActivatedRoute) {
    //console.log('processAffidFromRoute invoked!');
    const param = route.snapshot.queryParamMap.get('pid');
    //console.log('param: ', param);
    if (param) {
      const affid = param;
      await this.set(affid);
      //console.log('pid from get params processed: ', affid);
      return affid;
    } else {
      let stored = await this.get();
      if (stored !== null) {
        //console.log('pid from store processed: ', stored);
        return stored;
      } else {
        let affid = this.defaultPid;
        await this.set(affid);
        //console.log('default pid processed: ', affid);
        return affid;
      }
    }
  }

  public async set(value: string) {
    await this.storage.set('affid', value)
    //console.log('saved!', value);
  }

  public async get() {
    let res = await this.storage.get('affid')
    return res;
  }

}
