import { Component, OnInit } from '@angular/core';
import { CarsdbService } from '../carsdb.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Title, Meta } from '@angular/platform-browser';
import { AfflinkService } from '../afflink.service';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  customActionSheetOptions: any = {
    header: 'Производители'
  };
  carscount: any;
  updateddate: any;
  vendors: any;
  affid: any;
  constructor(
    private carsdb: CarsdbService,
    private afflink: AfflinkService,
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private title: Title,
    private meta: Meta,
  ) {

  }
  ngOnInit() {
    this.carsdb.countCars().subscribe(res => {
      this.carscount = res;
      this.title.setTitle('Каталог новых автомобилей в салонах официальных дилеров. Наличие, цены и отзывы');
      this.meta.addTag({
        name: 'description',
        content: 'Каталог из более чем ' + this.carscount + ' новых автомобилей в салонах официальных дилеров. Реальные цены, наличие, описание и отзывы'
      })
    });
    this.carsdb.updatedDate().subscribe(res => {
      console.log(res);
      this.updateddate = formatDate(res.created, 'dd.MM.yyyy', 'ru-RU');
      console.log(this.updateddate);
    });
    this.carsdb.getVendors().subscribe(res => this.vendors = res);
    (async () => {
      let affid = await this.afflink.processAffidFromRoute(this.activatedRoute);
      //console.log('this.affid', affid);
    })();
  }

  vendorPage(vendor) {
    alert(vendor);
  }

  OnChange(event: any) {
    let filtered_brand = event.detail.value;
    this.route.navigate(['/brand/' + filtered_brand]);
  }
}