import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarsdbService } from '../carsdb.service';
import { Title, Meta } from '@angular/platform-browser';
import { AfflinkService } from '../afflink.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.page.html',
  styleUrls: ['./brand.page.scss'],
})
export class BrandPage implements OnInit {
  customActionSheetOptions: any = {
    header: 'Модели'
  };
  vendor: string;
  brand_model: string;
  modelsforvendors: any;
  carssforvendor: any;
  offset = 0;
  carsCount: number;
  hasmore: boolean = true;
  constructor(
    private activatedRoute: ActivatedRoute,
    private carsdb: CarsdbService,
    private route: Router,
    private afflink: AfflinkService,
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    this.vendor = this.activatedRoute.snapshot.paramMap.get('vendor');
    this.brand_model = this.activatedRoute.snapshot.paramMap.get('brand_model');
    //console.log('this.vendor: ', this.vendor);
    //console.log('this.brand_model: ', this.brand_model);
    this.carsdb.getModelsForVendor(this.vendor).subscribe(res => this.modelsforvendors = res);
    this.carsdb.countCarsForVendor(this.vendor).subscribe(res => {
      this.carsCount = res;
      if (this.carsCount <= 10) { this.hasmore = false };
      this.title.setTitle('Реальные цены на ' + this.carsCount + ' новых автомобилей производства ' + this.vendor + ' в наличии у официальных дилеров');
      this.meta.updateTag({
        name: 'description',
        content: 'Реальные цены, наличие, описание и отзывы на ' + this.carsCount + ' новых автомобилей производства ' + this.vendor + ' в автосалонах официальных дилеров'
      })
    });
    this.carsdb.getCarsForVendor(this.vendor, this.offset).subscribe(res => this.carssforvendor = res);
    (async () => {
      let affid = await this.afflink.processAffidFromRoute(this.activatedRoute);
      //console.log('this.affid', affid);
    })();
  }

  loadMore() {
    if ((this.carsCount > this.carssforvendor.length) && (this.carsCount - this.carssforvendor.length) > 0) {
      this.offset = this.carssforvendor.length;
      this.carsdb.getCarsForVendor(this.vendor, this.offset).subscribe(res => {
        this.carssforvendor = this.carssforvendor.concat(res);
        this.carssforvendor.length == this.carsCount ? this.hasmore = false : null;
      });
    } else { this.hasmore = false }
  }

  OnChange(event: any) {
    let filtered_model = event.detail.value;
    this.route.navigate(['/model/' + this.vendor + '/' + filtered_model]);

  }

  openCar(objectId) {
    this.route.navigate(['/car/' + objectId]);
  }
}
