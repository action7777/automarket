import { TestBed } from '@angular/core/testing';

import { AfflinkService } from './afflink.service';

describe('AfflinkService', () => {
  let service: AfflinkService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AfflinkService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
