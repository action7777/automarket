import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarsdbService } from '../carsdb.service';
import { Title, Meta } from '@angular/platform-browser';
import { AfflinkService } from '../afflink.service';

@Component({
  selector: 'app-model',
  templateUrl: './model.page.html',
  styleUrls: ['./model.page.scss'],
})
export class ModelPage implements OnInit {
  vendor: string;
  brand_model: string;
  carssforvendor: any;
  offset: number = 0;
  carsCount: number = 0;
  hasmore: boolean = true;

  constructor(
    private activatedRoute: ActivatedRoute,
    private carsdb: CarsdbService,
    private route: Router,
    private afflink: AfflinkService,
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    this.vendor = this.activatedRoute.snapshot.paramMap.get('vendor');
    this.brand_model = this.activatedRoute.snapshot.paramMap.get('brand_model');
    //console.log('this.vendor: ', this.vendor);
    // console.log('this.brand_model: ', this.brand_model);
    this.carsdb.countCarsForModel(this.vendor, this.brand_model).subscribe(res => {
      this.carsCount = res;
      if (this.carsCount <= 10) { this.hasmore = false };
      this.title.setTitle('Найдено ' + this.carsCount + ' новых автомобилей «' + this.brand_model + '» ' + 'производства ' + this.vendor + ' в наличии в официальных автосалонах');
      this.meta.updateTag({
        name: 'description',
        content: 'Полный список из ' + this.carsCount + ' новых автомобилей «' + this.brand_model + '» ' + 'производства ' + this.vendor + ' в наличии в автосалонах официальных дилеров'
      })
    });
    this.carsdb.getCarsModelForVendor(this.vendor, this.brand_model, this.offset).subscribe(res => this.carssforvendor = res);
    (async () => {
      let affid = await this.afflink.processAffidFromRoute(this.activatedRoute);
      //console.log('this.affid', affid);
    })();
  }

  loadMore() {
    if ((this.carsCount > this.carssforvendor.length) && (this.carsCount - this.carssforvendor.length) > 0) {
      this.offset = this.carssforvendor.length;
      //console.log('this.offset ', this.offset);
      this.carsdb.getCarsModelForVendor(this.vendor, this.brand_model, this.offset).subscribe(res => {
        this.carssforvendor = this.carssforvendor.concat(res);
        this.carssforvendor.length == this.carsCount ? this.hasmore = false : null;
      });
    } else { this.hasmore = false }
  }

  openCar(objectId) {
    this.route.navigate(['/car/' + objectId]);
  }

}
