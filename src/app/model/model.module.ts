import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModelPageRoutingModule } from './model-routing.module';

import { ModelPage } from './model.page';
import { FallbackImgModule } from '../fallback-img/fallback-img.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModelPageRoutingModule,
    FallbackImgModule
  ],
  declarations: [ModelPage]
})
export class ModelPageModule { }
