import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'brand/:vendor',
    loadChildren: () => import('./brand/brand.module').then(m => m.BrandPageModule)
  },
  {
    path: 'model/:vendor/:brand_model',
    loadChildren: () => import('./model/model.module').then(m => m.ModelPageModule)
  },
  {
    path: 'car/:objectId',
    loadChildren: () => import('./car/car.module').then(m => m.CarPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
