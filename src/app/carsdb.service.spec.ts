import { TestBed } from '@angular/core/testing';

import { CarsdbService } from './carsdb.service';

describe('CarsdbService', () => {
  let service: CarsdbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarsdbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
