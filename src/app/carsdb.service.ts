import { Injectable } from '@angular/core';
import Backendless from 'backendless';
import { Observable, from } from 'rxjs';

const ShopsStore = Backendless.Data.of('carslist');

@Injectable({
  providedIn: 'root'
})
export class CarsdbService {

  constructor() { }


  countCars(): Observable<any> {
    const count: Promise<any> = ShopsStore.getObjectCount().then(count => { return count });
    return from(count);
  }

  updatedDate(): Observable<any> {
    const res: Promise<any> = ShopsStore.findFirst().then(res => { return res });
    return from(res);
  }

  getVendors() {
    var dataQueryBuilder = Backendless.DataQueryBuilder.create();
    dataQueryBuilder.setProperties("Count(objectId), vendor");
    dataQueryBuilder.setGroupBy("vendor");
    dataQueryBuilder.setPageSize(100).setOffset(0);
    const res: Promise<any> = ShopsStore.find(dataQueryBuilder).then(res => { return res });
    return from(res);
  }

  getModelsForVendor(vendor) {
    var whereClause = "vendor='" + vendor + "'";
    var dataQueryBuilder = Backendless.DataQueryBuilder.create().setWhereClause(whereClause);
    dataQueryBuilder.setProperties("Count(objectId), brand_model");
    dataQueryBuilder.setGroupBy("brand_model");
    dataQueryBuilder.setPageSize(100).setOffset(0);
    const res: Promise<any> = ShopsStore.find(dataQueryBuilder).then(res => { return res });
    return from(res);
  }

  getCarsForVendor(vendor, offset) {
    var whereClause = "vendor='" + vendor + "'";
    var dataQueryBuilder = Backendless.DataQueryBuilder.create().setWhereClause(whereClause);
    dataQueryBuilder.setProperties("name, price, picture, brand_model, param");
    dataQueryBuilder.setPageSize(10).setOffset(offset);
    const res: Promise<any> = ShopsStore.find(dataQueryBuilder).then(res => { return res });
    return from(res);
  }

  countCarsForVendor(vendor) {
    var whereClause = "vendor='" + vendor + "'";
    var dataQueryBuilder = Backendless.DataQueryBuilder.create().setWhereClause(whereClause);
    const count: Promise<any> = ShopsStore.getObjectCount(dataQueryBuilder).then(count => { return count });
    return from(count);
  }

  getCarsModelForVendor(vendor, model, offset) {
    var whereClause = "vendor='" + vendor + "' and brand_model='" + model + "'";
    var dataQueryBuilder = Backendless.DataQueryBuilder.create().setWhereClause(whereClause);
    dataQueryBuilder.setProperties("name, price, picture, brand_model, param");
    dataQueryBuilder.setPageSize(10).setOffset(offset);
    const res: Promise<any> = ShopsStore.find(dataQueryBuilder).then(res => { return res });
    return from(res);
  }

  countCarsForModel(vendor, model) {
    var whereClause = "vendor='" + vendor + "' and brand_model='" + model + "'";
    var dataQueryBuilder = Backendless.DataQueryBuilder.create().setWhereClause(whereClause);
    const count: Promise<any> = ShopsStore.getObjectCount(dataQueryBuilder).then(count => { return count });
    return from(count);
  }

  getCarById(objectId) {
    var whereClause = "objectId='" + objectId + "'";
    var dataQueryBuilder = Backendless.DataQueryBuilder.create().setWhereClause(whereClause);
    const res: Promise<any> = ShopsStore.find(dataQueryBuilder).then(res => { return res });
    return from(res);
  }
}
