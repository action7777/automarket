import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarPageRoutingModule } from './car-routing.module';

import { CarPage } from './car.page';
import { FallbackImgModule } from '../fallback-img/fallback-img.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarPageRoutingModule,
    FallbackImgModule,
    FontAwesomeModule
  ],
  declarations: [CarPage],
  bootstrap: [CarPage]
})
export class CarPageModule { }
