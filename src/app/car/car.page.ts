import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CarsdbService } from '../carsdb.service';
import { IonContent, AlertController } from '@ionic/angular';
import { faWhatsapp, faTelegramPlane } from '@fortawesome/free-brands-svg-icons';
import { Title, Meta } from '@angular/platform-browser';
import { AfflinkService } from '../afflink.service';
import { Browser } from '@capacitor/browser';


@Component({
  selector: 'app-car',
  templateUrl: './car.page.html',
  styleUrls: ['./car.page.scss'],
})
export class CarPage implements OnInit {
  faWhatsapp = faWhatsapp;
  faTelegramPlane = faTelegramPlane;
  car: string;
  carssforvendor: any;
  carname = '...';
  show35: boolean = false;
  affid: any;
  newregurl = '';
  @ViewChild('content', { static: false }) content: IonContent;
  constructor(
    private activatedRoute: ActivatedRoute,
    private carsdb: CarsdbService,
    private afflink: AfflinkService,
    public _zone: NgZone,
    public alertController: AlertController,
    private title: Title,
    private meta: Meta
  ) { }

  ngOnInit() {
    this.car = this.activatedRoute.snapshot.paramMap.get('objectId');
    this.carsdb.getCarById(this.car).subscribe(res => {
      this.carssforvendor = res[0];
      this.carname = this.carssforvendor.name;
      //console.log(this.carssforvendor);
      this.title.setTitle('Новый автомобиль ' + this.carssforvendor.name + ' ' + 'цвет ' + this.carssforvendor.param + ': наличие, цена и отзывы');
      this.meta.updateTag({
        name: 'description',
        content: 'Новый автомобиль в наличии: ' + this.carssforvendor.description
      })
    });

    (async () => {
      this.affid = await this.afflink.processAffidFromRoute(this.activatedRoute);
      //console.log('this.affid', this.affid);
    })();
  }

  showAT35() {
    this.show35 = true;
    this._zone.run(() => {
      setTimeout(() => {
        this.content.scrollToBottom(400);
      });
    });
  }

  next(slides) {
    slides.slideNext();
  }

  openSite(url) {
    //console.log('url to open: ', url);
    window.open(url, "_blank", "noopener");
  }

  async openUrl(urlToOpen) {
    console.log('trying to open: ', urlToOpen);
    await Browser.open({
      url: urlToOpen,
      toolbarColor: '#666'
    });
  }


  makeUrl() {
    let url = this.newregurl + this.affid;
    this.openSite(url);
  }

  async openDealer(url) {
    const alert = await this.alertController.create({
      header: 'Связь с дилером',
      message: 'Вы переходите на сайт дилера, где Вас ждет спецпредложение!',
      buttons: [
        {
          text: 'Отмена',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'OK',
          handler: () => {
            //console.log('Confirm Okay');
            this.openUrl(url);
          }
        }
      ]
    });

    await alert.present();
  }
}
