export const environment = {
  production: true,
  backendless: {
    APP_ID: 'XXXXXXXXXXXX', //backendless app id
    API_KEY: 'XXXXXXXXXXXX' //backendless REST API key
  }
};
