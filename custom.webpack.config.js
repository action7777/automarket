const webpack = require('webpack');
console.log('The custom config is used');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
    optimization: {
        minimize: true,
        minimizer: [
            new TerserPlugin({
                extractComments: true,
                cache: true,
                parallel: true,
                sourceMap: true,
                terserOptions: {
                    compress: {
                        drop_console: true, // will remove console.logs from your files
                        drop_debugger: true
                    },
                },
            }),
        ],
    },
};
