import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.automarketinfo',
  appName: 'cars-list',
  webDir: 'www',
  bundledWebRuntime: false
};

export default config;
